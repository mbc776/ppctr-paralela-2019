Practica 2

Mario Blanco Casado

3/12/2019

Prefacio

Esta practica me ha gustado porque al ser, tanto optimizacion paralela como secuencial, 
he encontrado muchos puntos en los que poder mejorar mi codigo y ha sido mas satisfactorio
que estar trabandome constantemente en la implementacion del codigo.

Índice

1.Sistema
2.Diseño e Implementación del Software
3.Metodología y desarrollo de pruebas realizadas
4.Discusión

1. Sistema

- Ejecución sobre Linux:
  - CPU Intel i5-7500 2.20GHz
  - CPU 6 cores: 1 socket, 1 threads/core, 4 cores/socket.
  - CPU 1645 MHz
  - 8 GiB RAM
  - Cache L3

2. Diseño e Implementación del Software

Al tener que hacer 3 implementaciones distintas en un solo fichero he utilizado instrucciones
de preprocesador para poder separar los distintos programas en la compilación y así poder
hacer un MAKE que lo haga todo a la vez. Los diferentes bloques están desordenados en el
código pero es indiferente para la ejecución.

En el bloque secuencial, he buscado reducir el número de instrucciones innecesarias que
ejecuta el procesador. Sobre todo me he centrado en optimizar la parte del bucle del método
sequential, que es donde más costo computacional acumula.

Lo primero que he hecho ha sido quitar las referencias a la estructura y guardarlas en una
variable local para que cada vez que necesite esos datos, tenerlos guardados en una variable
que tarde menos en acceder a ella. Además he eliminado los casts porque son innecesarios.
Las operaciones que se ejecutaban con una respuesta siempre igual ((double)1/(int)2.0) lo he
reducido a su solución (0.5) para no tener que ejecutarlo siempre cada vez que se hace una
iteración. La operación de la librería matemática pow, que se utiliza siempre para elevar algo al
cuadrado las elimino y sustituyo por su operación normal. Además he juntado las dos
operaciones en una sola sustituyendo valores. Por ultimo en este bloque he cambiado el valor
de i de int a double para que pueda ser desde 0.5 hasta el número de iteraciones más 0.5
porque al valor de las iteraciones siempre se le sumaba 0.5 por lo que se ahorran 2
operaciones de sumas en cada iteración.

Fuera del bucle también he realizado mejoras al código pero que no se aprecian en el
rendimiento final porque solo ahorran unas pocas operaciones al código.

Para el resto de bloques utilizo el código que he mejorado en secuencial.

En el bloque threads he hecho la paralelización del código buscando el mejor rendimiento
usando threads. Es algo diferente al resto porque he tenido que crear variables globales para
poder sacar la información del método que pasas al thread para que se ejecute porque no he
encontrado ninguna manera de que el método no sea void.

Lo primero que he hecho ha sido crear un array con N threads que pides por pantalla. Luego
les hago que empiecen ejecutarse.

En el método sequential que ejecuta cada thread, busco el id del thread que se está ejecutado
y lo comparo con cada uno que tengo en el array y cuando coincidan doy el número de la
posición del array que ejecutan a cada thread. Seguidamente calculamos la porción que cada
thread tiene que ejecutar con respecto a la posición en el array que ocupa. Cuando acabamos
de calcular esa porción la guardamos en el array global de resultados que tenemos. Entonces
de vuelta en el main hacemos el join para esperar a que todos los threads acaben. Cuando eso
sucede sumamos los resultados del array global, hacemos las últimas operaciones y sacamos
por pantalla el resultado.

El array global que declaro lo hago con 100 de tamaño porque considero que no voy a utilizar
nunca más de 100 threads para ejecutar el código porque no hay mejora sustancial en el
tiempo de ejecución.

En el bloque de MPI lo que hago básicamente es paralelizar el código utilizando directivas
openMP pero con algunas restricciones.

Creo un pragma dentro del método sequential que crea el máximo número de threads y pongo
como privada la variable i porque al utilizarse como variable contadora del bucle es necesario
que cada thread tenga una copia de ella y no sea compartida, además evita que se pierdan
ciclos si dos threads quieren escribir en ella a la vez. Además he utilizado la cláusula reducción
para tmp con operación de suma, esto lo que hace es tratar a tmp como variable privada y
realizar la operación que se le indica al final para en este caso sumar los valores resultantes de
cada thread. El resto de variables son comunes y no es necesario especificarlas.

Dentro de este pragma al no poder utilizar el pragma for, he decidido utilizar métodos de
openMP. Los métodos que he utilizado me ayudan a saber que numero de thread estoy
ejecutando y con eso darle un trozo de la ejecución del bucle. El resultado le sumo para
obtener el dato que quiero.



3. Metodología y desarrollo de las pruebas realizadas

He utilizado el comando time en la terminal para medir el tiempo y luego lo he comparado.

Siempre he utilizado 1.000.000.000 iteraciones para la ejecución.

La primera medición con tiempo secuencial con el código inicial sale a una media de 17,842
segundos, mientras que mi optimización en secuencial lo hace en 2,883 por lo que lo se
obtiene un speedup de 6,18.

La optimización de mi secuencial con respecto a la paralelización con threads y en MPI es la
siguiente:

‐Threads: se obtiene una mejora en 2 threads de 1,972 de speedup, en 3 de 2,868, en 4 de
2,909. Luego subí drásticamente la cantidad de threads que se utilizan hasta 50, obteniendo un
speedup de 3,783 y con 100 de 3,808. Hice pruebas con más cantidad de threads pero no
obtuve ninguna mejora significativa.

‐MPI: se obtiene una mejora en 2 hilos de 1,963 de speedup, en 3 de 2,909 y en 4 de 3.838



4. Discusión

Esta práctica me ha servido para refrescar conceptos de openMP que tenía olvidados, me he
encontrado con dificultades que ya había superado pero no me acordaba de cómo resolver por
lo que me ha sido de ayuda realizar esta práctica.
Además como no hemos utilizado todas las directivas openMP disponibles (for sobretodo), he
tenido que pensar soluciones alternativas a problemas que ya sabía resolver.
