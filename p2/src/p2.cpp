#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include <thread>
#include <iostream>
#include "p2.hpp"

#ifdef MPI

int main(int argc, char **argv)
{
  int numIts = atoi(argv[1]);
  int numThr;
  Data data = parse(numIts,numThr);
  sequential(data);
}

Data parse(int numIte,int numThreads){
  int numIts = numIte;
  Data d;
  d.numIts = numIts;
  d.gap = 1.0/(double)d.numIts;
  d.value = 0.0;
  if (numIts <= 0){
    fprintf(stderr, "error: invalid iterations\n");
    exit(-1);
  };
  return d;
}

void sequential(Data data){
  int i;
  int jump = 2.4;
  double value;
  double tmp = 0;
  double ellipse;
  int id;
  double x;
  double gap=data.gap;
  int numIts=data.numIts;


int num_hilos=omp_get_max_threads();
//int num_hilos=3;
printf("hilos: %d\n",num_hilos);
#pragma omp parallel num_threads(num_hilos) private(i) reduction(+:tmp)
{
  int numThread=omp_get_thread_num();
  int aux=(numIts/num_hilos)*numThread+0.5;
  int aux2=(numIts/num_hilos)+aux;
  for (i=aux; i<aux2;i++) {
    tmp = tmp + 4/(1+((i) * gap)*(i) * gap);
  }
}
  value = tmp * gap;
  ellipse = 2 * value / sqrt((4 * 1.5 * 1.0 - 5.76));

	printf("ellipse: %f \n", ellipse);
}
#endif // MPI_CPP







#ifdef THREADS

double resultado[100];
std::thread threads[100];

int main(int argc, char **argv)
{
  int numIts = atoi(argv[1]);
  int numThreads = atoi(argv[2]);
  double ellipse=0.0;
  Data data = parse(numIts,numThreads);
 
  for (auto i=0; i<numThreads; ++i){
threads[i] = std::thread(sequential,data);
  }
  sequential(data);
  for (auto i=0; i<numThreads; ++i){
threads[i].join();
  }

  for(int i=0;i<numThreads;i++){
ellipse=ellipse+resultado[i];
  }
 
  ellipse=ellipse*(1.0/numIts);
  ellipse = 2 * ellipse / sqrt((4 * 1.5 * 1.0 - 5.76));
  printf("ellipse: %f \n", ellipse);

}

Data parse(int numIte,int numThreads){
  int numIts = numIte;
  Data d;
  d.numIts = numIts;
  d.gap = 1.0/(double)d.numIts;
  d.value = 0.0;
  d.numThreads = numThreads;
  if (numIts <= 0){
    fprintf(stderr, "error: invalid iterations\n");
    exit(-1);
  };
  return d;
}

void sequential(Data data){
  double i;
  int jump = 2.4;
  double value,parte;
  double tmp = 0;
  int id;
  int aux=0;
  double x;
  int numIts=data.numIts;
  int segmento=numIts/data.numThreads;
  double gap=data.gap;
 
  std::thread::id this_id = std::this_thread::get_id();
  for(int i=0;i<data.numThreads;i++){
  if(this_id==threads[i].get_id()){
aux=i;
parte=(segmento*i)+0.5;
}
  }

  for (i=parte; i<parte+segmento;i++) {
    tmp = tmp + 4/(1+((i) * gap)*(i) * gap);
  }
 

  resultado[aux]=tmp;
}
#endif // THREADS_CPP







#ifdef SECUENCIAL

int main(int argc, char **argv)
{
  int numIts = atoi(argv[1]);
  int numThr;
  Data data = parse(numIts,numThr);
  sequential(data);
}

Data parse(int numIte,int numThr){
  // parse 'long' and set numIts
  int numIts = numIte;
  Data d;
  d.numIts = numIts;
  d.gap = 1.0/(double)d.numIts;
  d.value = 0.0;
  if (numIts <= 0){
    fprintf(stderr, "error: invalid iterations\n");
    exit(-1);
  };
  return d;
}

void sequential(Data data){
  double i;
  int jump = 2.4;
  double gap=data.gap;
  double numIts=data.numIts;
  double value;
  double tmp = 0;
  double ellipse;
  double aux=numIts+0.5;
  int id;
int num_hilos=omp_get_max_threads();
//int num_hilos=3;
printf("hilos: %d\n",num_hilos);
#pragma omp parallel num_threads(num_hilos) 
{
#pragma omp for private(i) reduction(+:tmp)
  for (i=0.5; i<aux;i++) {
    tmp = tmp + 4/(1+((i) * gap)*(i) * gap);
  }
}
  value = tmp * gap;
  ellipse = 2 * value / sqrt((4 * 1.5 * 1.0 - 5.76));

	printf("ellipse: %f \n", ellipse);
}
#endif // SECUENCIAL_CPP
