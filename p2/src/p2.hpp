#ifndef P2_HPP
#define P2_HPP

struct Aggregator {
  double separator;
  double A;
  double B;
  double C;
};

struct Data {
  double value;
  double gap;
  int numThreads;
  long numIts;
};

void sequential(Data data);
Data parse(int numIte,int numThreads);
int attach(Data* data, long iterations);

#endif // P2_HPP

