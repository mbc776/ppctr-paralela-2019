#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <condition_variable>
#include <mutex>
#include <thread>


struct ControlLogger{
	int id_thread;
    int trabajoLogger;
    double canal;
    std::mutex mutex;
    std::condition_variable condition_variable;
	std::mutex mutexResultado;
    std::condition_variable condition_variableResultado;
};

typedef struct ControlLogger ControlLogger;

struct ControladorRegistros{
    int *threads;
    std::mutex mutex;
};

typedef struct ControladorRegistros ControladorRegistros;

void logger(int numHilos, int tipoOp, ControlLogger *controlador, ControladorRegistros *registro, double *resLogger);
void reductor(double *array, int base, int trabajo, int tipoOp,int id, ControlLogger *controlador, ControladorRegistros *registro, double *resultadosHilos);


int main(int argc, char **argv){
	int tipoOp,numHilos = 1;
	
  	if(argc == 4 || argc < 3 || argc > 5){
  		printf("Error: N?mero de parametros incorrecto\n");
  		return 0;
  	}
	
  	int numIts = atoi(argv[1]);
	
  	if (numIts == 0 && strcmp(argv[1], "0") != 0){
  		printf("Error: Longitud de array no es un n?mero\n");
  		return 0;
  	}
	
  	if(strcmp(argv[2],"xor")==0){
          tipoOp = 0;
  	}else if(strcmp(argv[2],"sum")==0){
          tipoOp = 1;
  	}else if(strcmp(argv[2],"sub")==0){
		  tipoOp = 2;
	}else{
  		printf("Error: No existe esa operaci?n\n");
  		return 0;
  	}
	
  	if(argc == 5){
		
  		if(strcmp(argv[3], "--multi-thread") != 0){
  			printf("Error: Flag incorrecta\n");
  			return 0;
  		}
		
  		numHilos = atoi(argv[4]);
		
  		if (numIts == 0 && strcmp(argv[1], "0")!=0){
  			printf("Error: N?mero de threads no es un n?mero\n");
  			return 0;
  		}
		
  		if(!(numHilos>0 && numHilos<10)){
  			printf("Error: N?mero de threads invalido\n");
  			return 0;
  		}
    }

	double *array = (double*) malloc(numIts*sizeof(double));
	
	for(int i = 0; i<numIts; i++){
		array[i] = (double)i;
	}

    struct timeval tInicio, tFin;

  if(numHilos>1){
    double *resultadosHilos = (double*)malloc(numHilos*sizeof(double));
    double resLogger = 0;
	ControladorRegistros registro;
    ControlLogger controlador;
    std::thread threadLogger;
    registro.threads = (int*) malloc((numHilos+1) * sizeof(int));
    registro.threads[0] = 1;


    #ifdef FEATURE_LOGGER
      threadLogger = std::thread(logger, numHilos ,tipoOp, &controlador, &registro, &resLogger);
      controlador.trabajoLogger = 0;
    #endif

    std::thread hilos[numHilos];
	int base = 0;
  	int trabajo = floor(numIts/numHilos);
    int trabajoDeMas = numIts - trabajo*numHilos;
    

    gettimeofday(&tInicio, NULL);
	
    for(int i = 0; i<numHilos; i++){
      if(i != 0){
        hilos[i] = std::thread(reductor,array, base,trabajo, tipoOp, i, &controlador, &registro, resultadosHilos);
        base += trabajo;
      }else{
        hilos[i] = std::thread(reductor, array, base, trabajo + trabajoDeMas,tipoOp, i, &controlador,&registro, resultadosHilos);
        base += trabajo + trabajoDeMas;
      }
    }
	
    double resultado = 0;
	
    for (int i = 0; i<numHilos; ++i){
      hilos[i].join();
      if(tipoOp==0){
		resultado = (long long)resultado xor (long long)resultadosHilos[i];
      }else if(tipoOp==1){
        resultado += resultadosHilos[i];
      }else{
		resultado -= resultadosHilos[i];
	  }
    }
	
    gettimeofday(&tFin, NULL);

    #ifdef DEBUG
      printf("Resultado final: %f\n", resultado);
    #endif
	
    #ifdef FEATURE_LOGGER
        if(!resLogger){
            std::unique_lock <std::mutex> ulk( controlador.mutexResultado);
            controlador.condition_variableResultado.wait( ulk, [&]{return !resLogger;});
            ulk.unlock();
        }
		
        threadLogger.join();
        printf("Resultado del logger: %f\n", resLogger);
    #endif

  }else{
    gettimeofday(&tInicio, NULL);
    reductor(array, 0, numIts,tipoOp, 0, NULL,NULL, NULL);
    gettimeofday(&tFin, NULL);
  }
  
  double seg, mseg, auxInicio, auxFin;
  seg = tInicio.tv_sec;
  mseg = tInicio.tv_usec;
  auxInicio = seg*1000 + mseg/pow(10.0, 3.0);
  seg = tFin.tv_sec;
  mseg = tFin.tv_usec;
  auxFin = seg*1000 + mseg/pow(10.0, 3.0);
  printf("%f\n", auxFin - auxInicio);
  return(1);
}

void logger(int numHilos, int tipoOp, ControlLogger *controlador, ControladorRegistros *registro, double *resLogger){
	
    int hilosRestantes = numHilos;
    double resultado = 0;
    double resultadoHilos[numHilos];

    while(hilosRestantes != 0){
      std::unique_lock<std::mutex> ulk(controlador->mutex);
      #ifdef DEBUG
        printf("Espero a un thread\n");
      #endif
      controlador -> condition_variable.wait(ulk, [&]{return controlador -> trabajoLogger;});
      controlador -> trabajoLogger = 0;
      resultadoHilos[controlador -> id_thread] = controlador -> canal;
      #ifdef DEBUG
        printf("Resuelvo thread %d\n", controlador -> id_thread);
      #endif
      ulk.unlock();
      controlador -> condition_variable.notify_all();
      hilosRestantes--;
    }

    for(int i = 0; i<numHilos; i++){
      printf("Thead %d: %f\n", i, resultadoHilos[i]);
      switch (tipoOp){
        case 0:
          resultado = (long long)resultado xor (long long)resultadoHilos[i];
          break;
        case 1:
          resultado += resultadoHilos[i];
          break;
		case 2:
		  resultado -= resultadoHilos[i];
		  break;
      }
    }
	
    *resLogger = resultado;
    controlador -> condition_variableResultado.notify_all();
	
    for (int i=1;i<numHilos+1;i++){
        printf("%d : thread %d\n", i, registro->threads[i]);
    }
}

void reductor(double *array, int base, int trabajo, int tipoOp, int id, ControlLogger *controlador, ControladorRegistros *registro, double *resultadosHilos){
    double counter  = 0;
    for(int i = base; i<base+trabajo; i++){
        switch(tipoOp){
            case 0:
                counter = (long long)counter xor (long long)array[i];
                break;
            case 1:
                counter += array[i];
                break;
			case 2:
				counter -= array[i];
				break;
        }
    }
	
    if(resultadosHilos!=NULL){
        resultadosHilos[id] = counter;
    }

    #ifdef FEATURE_LOGGER
      if(controlador != NULL){
        std::unique_lock<std::mutex> ulk(controlador->mutex);
        controlador->id_thread = id;
        controlador->condition_variable.wait(ulk, [&]{return !controlador->trabajoLogger;});
		controlador->canal = counter;
        controlador->trabajoLogger = 1;
        ulk.unlock();
        controlador->condition_variable.notify_all();
      }
	  
      if(registro != NULL){
        std::lock_guard<std::mutex> guard(registro->mutex);
		registro->threads[0]++;
        registro->threads[registro->threads[0]] = id;
      }
    #endif
    
    #ifdef DEBUG
        if(registro == NULL)
          printf("Resultado final: %f\n", counter);
    #endif
}



