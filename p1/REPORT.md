PRACTICA 1

Mario Blanco Casado

3/12/2019

Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

1. Sistema

- Ejecución sobre Linux:
  - CPU Intel i7 8750M 2.20GHz
  - CPU 12 cores: 1 socket, 2 threads/core, 6 cores/socket.
  - CPU 1400 MHz
  - 8 GiB RAM
  - Cache L3


2. Diseño e Implementación del software

El programa está compuesto por tres funciones: Main, Reductor y Logger

El main recibe como está especificado la longitud del array, la operación a realizar y por último,
de manera opcional –multi‐thread y el número de threads a ejecutar.

Lo primero que hace el main cuando se empieza a ejecutar es comprobar si los parámetros
introducidos están en el rango de los posibles, si no es así manda un mensaje de error y finaliza
la ejecución.

Después se inicializaran las variables necesarias. En este punto si el programa es secuencial, la
reducción se hará secuencialmente y se realizaran las mediciones normalmente. En caso de ser
paralelo se inicializan variables adicionales necesarias para el correcto funcionamiento y se
crearan los hilos a los cuales se les asignara un trozo del problema para ejecutar, después se
esperara a que terminen todos los hilos y se aplicara la operación a los resultados obtenidos.

La función logger recibe los resultados de los hilos, para lo cual utiliza una variable condicional
que indica si el logger está ocupado o no. Al inicializarse la función, se pone a libre
automáticamente y espera a que un hilo le mande una petición. Cuando le mandan unapetición se pone ocupado, con lo que puede operar el resultado, marcarse como disponible,
avisar al resto de hilos y ponerse otra vez en espera.

Una vez finalizados los hilos, sacara por pantalla el resultado de los mismos y se los pasa al
main. Por ultimo imprime el orden de finalización de los hilos.

La función reductor aplica la operación al array y devuelve los resultados obtenidos. Si además
el logger está activo se guarda el orden de finalización de los hilos y le ira pasando al logger el
resultado de cada hilo.

Para poder escribir en el registro, se ha de tomar un mutex, porque cada hilo que finaliza
escribe su id en el registro de la posición marcada por un index compartido, y luego avanza en
el index.

Como queremos pasar los resultados al logger, utilizo una variable condicional para saber si el
hilo logger está preparado para recibir. Una vez que se sabe que el logger está preparado para
recibir, uno de los hilos que quieren recibir se desbloquea, envía su resultado, marca al hilo
logger como ocupado, libera el mutex y notifica al resto de hilos y al logger. Por el otro lado si
no está listo, espera hasta que lo este.

El modo que tienen de funcionar los hilos es que les corresponde la longitud del array dividido
entre el número de hilos. Si la división no sale redonda, el primer hilo que termine de ejecutar
su trabajo ejecuta el trabajo restante.

3. Metodología y desarrollo de las pruebas realizadas

Para hacer las prubas he utilizado gettimeofday como se especifica. Además he utilizado el
script benchsuite.sh para el benchmarking como se especifica en el enunciado. Como XOR es la
operación con más coste computacional, he utilizado esa para hacer las pruebas.
Después de hacer las pruebas, el programa en paralelo con respecto al secuencial tiene un
speedup de 4 con 8 threads y de 3,66 con 4 threads. Por lo que claramente se ve que con más
threads más mejora la ejecución, pero no hasta el infinito, ya que con un número muy alto de
threads, el tiempo de creación, manipulación y finalización de los mismos sería más alto que la
mejora obtenida de utilizarlos.

4. Discusión

Esta práctica me ha resultado bastante complicada pero me ha servido para recordar
conceptos de Sistemas Operativos que tenía olvidados y además aprender cosas nuevas que
no había hecho nunca como programación en c++, uso de operaciones de preprocesador,
GIT...
