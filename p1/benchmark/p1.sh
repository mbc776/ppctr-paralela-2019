#!/usr/bin/env bash
size=51200000
op=xor
nthreads1=8
nthreads2=4
case "$1" in
    sec)
        ./../build/p1 $size $op
        ;;
    par8)
        ./../build/p1 $size $op --multi-thread $nthreads1
        ;;
    par4)
        ./../build/p1 $size $op --multi-thread $nthreads2
        ;;
esac
