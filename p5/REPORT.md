Practica 5

Mario Blanco Casado

Fecha 17/12/2019

Prefacio

En esta práctica no he encontrado grandes dificultades al resolverla porque ya tenía soltura de prácticas anteriores, no obstante sí que he tenido alguna dificultad en resolver algún punto de la actividad 3.
En general me ha servido para continuar mi aprendizaje

- Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

1. Sistema

- Ejecución sobre Linux:
  - CPU Intel i7 8750M 2.20GHz
  - CPU 12 cores: 1 socket, 2 threads/core, 6 cores/socket.
  - CPU 1400 MHz
  - 8 GiB RAM
  - Cache L3

2. Diseño e Implementación del Software

Dependiendo del ejercicio he realizado una implementación diferente.

Para el primero he utilizado la función time_get_wtime() porque es la función que más me gusta para hacer profiling y con la que mejor me manejo.

En el segundo ejercicio he utilizado directivas de openMP para mejorar la eficiencia, el por qué esta en la discusión del ejercicio 2.

En el ejercicio 3 he utilizado diferentes estrategias para resolver las problemáticas dadas, la explicación de cada una está en su apartado de la discusión.

3. Metodología y desarrollo de las pruebas realizadas

Las pruebas que he realizado para el rendimiento utilizo la función time_get_wtime() y el comando time en la terminar para determinar los tiempos de ejecución de los diferentes programas, métodos, etc...
Siempre utilizo 4001 pixeles y 1000 iteraciones.
Con respecto al tiempo global de optimización he logrado reducir de 28,432 segundo de ejecución secuencial a 7,411 en el mejor caso de optimización paralela con 12 cores, lo que supone un speedup de 3,8364.
Con respecto al ejercicio 3 hay 2 implementaciones que destacan sobre el resto, estas son SEC y PRIVATE con un rendimiento muy parecido y esta última con muy poca perdida con respecto al secuencial, las otras 2 tienen una pérdida de rendimiento muy grande a causa de que la mejora que producen al paralelizarse en muy pequeña con respecto a todo el tiempo que se pierde en la gestión de los threads. Hay un hilo mostrando la perdida de rendimiento con respecto al speedup en la imagen ejercicio4.png.

4. Discusión

Ejercicio 1

Para este apartado he utilizado la herramienta que proporciona omp para medir tiempos (omp_get_wtime).
La he implementado en la versión secuencial del código para poder ver mejor en qué lugares se gasta más tiempo de ejecución.

Empezando por las funciones:
La función explode se ejecuta muchas veces con un tiempo ínfimo de ejecución por llamada, por lo que es importante la optimización.
La función write_header es no es importante optimizarla porque solo se llama una vez y tiene un tiempo de ejecución bajo
La función write_data es más importante su optimización porque tarda un par de segundos en ejecutarse aunque al tratarse de una función que escribe en memoria, es de difícil optimización.
La función write también tarda pero es básicamente porque es la suma de las dos funciones anteriores.

Con respecto a la función main, nos encontramos 4 regiones principales de código que coinciden con las regiones a paralelizar más adelante.
La más importante y la que más región tiempo de cómputo cuesta es el cálculo del COUNT. Más de 3/4 del tiempo de la ejecución en secuencial se gastan en esta región. Es paralelizable por lo que será de vital importancia la paralización de este segmento
La siguiente zona de importancia es el bucle que determina el coloreado de cada pixel. Esta zona tarda menos de medio segundo en ejecutarse, con lo que no es de vital importancia su optimización, pero aun así es paralelizable.
Por ultimo está el setteo de la imagen por parte de un último bucle for que tarda algo más de medio segundo en ejecutarse en secuencial, por lo que como la anterior zona, no es de vital importancia su paralelización, aunque al tratarse de un bucle es bastante sencillo.

Ejercicio 2

1) Utilizo la directiva dynamic que reparte la carga entre los bucles de forma dinámica, es decir, las iteraciones se dividen en bloques del tamaño especificado y se asignan dinámicamente a los threads cuando van acabando su trabajo.

2) i,j,x,y,c son privadas porque no necesitamos su valor inicial y no son compartidas. x_min, x_max, y_min, y_max, n, count_max, count, c_max, r, g, b son shared porque da igual que varios threads las utilizan al tiempo porque puede necesitarse su valor inicial o porque son arrays de escritura o lectura compartidos por varios threads.

3) Como he explicado antes en el código hay una gran región paralelizable en el main que consta de varios bucles de coste computacional importante. Por ello hago las siguientes optimizaciones:
Utilizar un bucle for schedule dynamic porque al ser este un bucle con carga desigual, esto es, que las primeras iteraciones del bucle son menos costosas que las finales, es mejor utilizar esta directiva que se ajusta más a las necesidades del código.
El segundo bucle lo explicare en el siguiente ejercicio.
La siguiente parte corresponde a una directiva single que hace ejecutar de forma secuencial el código de los mallocs porque si cada thread lo ejecutara, se crearían muchos más huecos en memoria de los necesitados y además perdería eficiencia.
Por último, el último bucle for es uno normal. Solo tiene una variable privada para que funcione bien.

Ejercicio 3

Para este ejercicio he utilizado las directivas del preprocesador para poder separar las diferentes implementaciones en el mismo código. Hay que tener en cuenta que una parte de este bucle no se puede ejecutar en paralelo por lo que hay que respetarlo y de ahí vienen las diferentes maneras de implementarlo.

a) En esta implementación rodeo la región critica con un pragma omp critical para que cada los threads solo puedan pasar de uno en uno, porque es lo que se consigue con esta función.

b) Con las funciones del runtime utilizo un lock que creo al principio con todas las variables y utilizo para que se comporte igual que un pragma critical y por lo tanto solo deje pasar de uno en uno a los threads en su ejecución.

c) Para esta función solo hace falta un pragma omp single, porque al estar ejecutándose en una región paralela, si no se utiliza esta función no funcionaria. Este pragma lo que hace es que solo un thread pueda ejecutar esa región del código por lo que cumpliría la función de que la región critica solo se ejecute por un thread.

d) Esta implementación me ha parecido la más complicada y se lleva a cabo por la implementación de una variable auxiliar que cada thread ejecuta y luego fuera del bucle en una región critica, se saca la variable que realmente es la buena, para que los hilos no se pisen entre ellos.

