#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include <string.h>
#include <omp.h>

void fgauss (int *, int *, long, long);




int main(int argc, char *argv[]) {

    FILE *in;
    FILE *out;
    int i, j, size, seq = 8;
    int **pixels, **filtered;

    //chdir("/tmp");
    in = fopen("movie.in", "rb");

    if (in == NULL) {
        perror("movie.in");
        exit(EXIT_FAILURE);
    }

    out = fopen("movie.out", "wb");

    if (out == NULL) {
        perror("movie.out");
        exit(EXIT_FAILURE);
    }

    int width, height;

    fread(&width, sizeof(width), 1, in);
    fread(&height, sizeof(height), 1, in);

    fwrite(&width, sizeof(width), 1, out);
    fwrite(&height, sizeof(height), 1, out);

    pixels = (int **) malloc (seq * sizeof (int *));
    filtered = (int **) malloc (seq * sizeof (int *));

    for (i=0; i<seq; i++){
        pixels[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
        filtered[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
    }

#ifdef PAR
        int aux = -1;
	int countImagenes = 0;
        double inicial = omp_get_wtime();
	//int numHilos = omp_get_max_threads();
	int numHilos=2;
	printf("hilos: %d\n",numHilos);
        #pragma omp parallel private(size), shared(i, aux, pixels, filtered, height, width, countImagenes) num_threads(numHilos)
        {
            i = 0;
            #pragma omp master
            {
                do{
                    if(i<seq){
                        size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);
                        if(size){
                            #pragma omp task
                            {
                                int pos;
                                #pragma omp critical
                                {
                                    aux++;
                                    pos = aux;
                                }
                                fgauss (pixels[pos], filtered[pos], height, width);
                            }
                            i++;
                        }
                    }
                    else{
                        #pragma omp taskwait
                        for(j=0; j<seq; j++){
                            fwrite(filtered[j], (height+2) * (width + 2) * sizeof(int), 1, out);
                            i = 0;
                            aux = -1;
                        }
                    }
                }while (!feof(in));
            }
            #pragma omp taskwait
        }


        for(i = 0; i<=aux; i++){
            fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
        }
        printf("Tiempo: %f\n", omp_get_wtime()-inicial);
#endif // PAR
#ifdef SEC 
	i = 0;
        j=0;
        double inicial = omp_get_wtime();
        do {
            size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);
            j++;
            if (size){
                fgauss (pixels[i], filtered[i], height, width);
                fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
            }     
        } while (!feof(in));
        printf("Tiempo: %f\n", omp_get_wtime()-inicial);
#endif //SEC
    for (i=0; i<seq; i++){
        free (pixels[i]);
        free (filtered[i]);
    }
    free(pixels);
    free(filtered);

    fclose(out);
    fclose(in);

    return EXIT_SUCCESS;
}

void fgauss (int *pixels, int *filtered, long heigh, long width){
	int y, x, dx, dy;
	int filter[5][5] = {1, 4,  6,  4,  1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4,  1, 4,  6,  4,  1};
	int sum;

	for (x = 0; x < width; x++) {
		for (y = 0; y < heigh; y++){
			sum = 0;
			for (dx = 0; dx < 5; dx++)
				for (dy = 0; dy < 5; dy++)
					if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
						sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
			filtered[x*heigh+y] = (int) sum/273;
		}
	}
}


