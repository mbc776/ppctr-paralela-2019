PRACTICA 4

Mario Blanco Casado

3/12/2019

Prefacio

En esta practica he aprendido los objetivos que se me pedian y la he realizado con cierta soltura 
teniendo en cuenta las dificultades que se me han presentado.
Me ha servido para seguir aprendiendo.

Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

1. Sistema

- Ejecución sobre Linux:
  - CPU Intel i7 8750M 2.20GHz
  - CPU 12 cores: 1 socket, 2 threads/core, 6 cores/socket.
  - CPU 1400 MHz
  - 8 GiB RAM
  - Cache L3


2. Diseño e Implementación del software

He hecho 2 implementaciones del sistema, secuencial y paralela, para utilizar cada una utilizamos 
directivas del preprocesador.

En la implementacion en secuencial, no canbiamos nada, solo ponemos un par de lineas para medir el
rendimiento del codigo.

En la implementacion en paralelo, aparte de poner el codigo necesario para medir el rendimiento del
codigo se implementa más codigo como el que esta al lado de la directiva critical, pero esto se explica
más adelante en el informe.

3. Metodología y desarrollo de las pruebas realizadas

Como acabo de explicar en el apartado anterior, he puesto unas directivas de codigo para medir el
rendimiento usando omp_get_wtime(), al principio y al final de la zona que queria medir el rendimiento.
Cuando acabo, resto sus valores y lo imprimo por pantalla para saber su valor

He realizado varias mediciones de las dos implementaciones en secuencial y paralelo, y me sale que
de media la implementacion secuencial tarda 4.104 segundos y 0.761 la paralela con 12 hilos.
En TiempoEjecucion.png hay una grafica con los resultados y los speedups más detallados
Todas estas medidas estan tomadas en el directorio tmp

4. Discusión

1) 

El programa ahora dependiendo de como lo compiles, puedes ejecutarlo de manera secuencial o paralela.
Esto se ha conseguido por medio de directivas de preprocesador

2)

master: 
Directiva que sirve para que solo ejecute el segmento de codigo especificado el hilo master
En este caso el thread maestro va creando las tareas que el resto de threads del equipo van ejecutando

task:
Una vez que se llegua a la directiva task, el código no será ejecutado inmediatamente, si no que
será puesto en una cola como tarea pendiente. Cada vez que un hilo termina con una tarea pasará a la 
siguiente que este pendiente.

critical:
En este segmento habra un mutex asociado, el hilo que este el poder del mutex podra ejecutar la sección 
crítica, y cuando lo libere avisara al resto de hilos para que si alguien quiere, pueda tomarlo.

taskwait: 
Esta directiva crea una barrera explicita, bloquea los hilos hasta que todos hayan llegado a ese punto.


3)

Size es privado porque no necesitamos su valor inicial y es compartida por los hilos. Las variables
i, aux, pixels, filtered, height, width, countImagenes son compartidas ya que o se comparten entre
los hilos (i, aux, countImagenes ), o se necesita su valor inicial (height, width ), o son arrays de 
lectura/escritura compartidos (pixels, filterred ).


4)

El paralelismo se consigue repartiendo el trabajo en 2 tipos de hilos:
-El hilo master que hace la lectura y escritura del fichero y ademas crea los task a ejecutar por el
resto de hilos esclavos
-El hilo esclavo hace el computo de las operaciones que le manda el master (aplica el filtro).
Cuando han acabado todas las operaciones, escribe en el archivo las imagenes filtradas


5)

Hay una grafica mostrando los resultados de los speedups en SpeedUp.png
